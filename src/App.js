import UploadFile from "./Components/UploadFile"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import NavBar from "./Components/Navbar"
import ShowVc from "./Components/ShowVc"
import FormAndQr from "./Pages/FormAndQR.js"

function App() {
  return (
    <Router>
      <div className='content'>
        <NavBar />
        <Switch>
          <Route exact path='/'>
            <UploadFile />
          </Route>
          <Route exact path='/showvc'>
            <ShowVc />
          </Route>
          <Route exact path='/vacine-cred'>
            <FormAndQr />
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App
