import { useSelector } from "react-redux"
// import { useState } from "react"
import { library } from "@fortawesome/fontawesome-svg-core"
import { fas } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const ShowVc = () => {
  library.add(fas)
  const verifyableCredential = useSelector((state) => state.vcReducers)

  // const renderVc = () => {
  //   const data = []
  //   if (verifyableCredential.vc !== null) {
  //     const vc = verifyableCredential.vc.credentialSubject
  //     for (const [key, value] of Object.entries(vc)) {
  //       data.push(`${key} : ${value}`)
  //     }
  //   }
  //   return data
  // }

  // const [vclist] = useState(renderVc)

  return (
    <div className='container'>
      {/* Section for for logo name */}
      <div className='row'>
        <div className='col-md-6'>
          <img
            src='https://scontent.fbkk7-2.fna.fbcdn.net/v/t1.6435-9/121739101_2875054282815503_281115247499878320_n.jpg?_nc_cat=110&ccb=1-3&_nc_sid=09cbfe&_nc_eui2=AeGUvK9aAzw09PVIo7FAJZVVtkyGz4g3b9-2TIbPiDdv34-gAQwhVws4nzoNOxmpGJLf0rjkO4PPDPaxshIodhEB&_nc_ohc=UWWNNLgIzoEAX9x8k3Y&_nc_ht=scontent.fbkk7-2.fna&oh=0604389ba59e3be21451b14bb586427a&oe=6089B529'
            alt=''
            width='50%'
          />
        </div>
        <div className='col-md-6'>
          <p>Test Testlastname</p>
          <p>Gender: Male Age: xx Passport: xx-xxx-xxx</p>
        </div>
      </div>

      {/* Section for Company and vacine name */}
      <div className='row'>
        <div className='col-md-6'>
          <div
            style={{
              border: "2px solid black",
              width: "100%",
              padding: "3rem",
            }}
          ></div>
          <p>amount: 2/2</p>
        </div>
        <div className='col-md-6'>
          <p>Sinovac Biotech</p>
          <p>"Corona Vac"</p>
        </div>
      </div>

      {/* Section for Vacine vc detail*/}
      <div className='row'>
        <div className='col-md-6'>
          <FontAwesomeIcon
            style={{ width: "40%", height: "40%" }}
            icon={["fas", "share"]}
          />
        </div>
        <div className='col-md-6'>
          <div className='row'>
            <div className='col-md-6'>
              {verifyableCredential.iss !== null ? (
                <p>iss: {verifyableCredential.iss}</p>
              ) : null}
            </div>
            <div className='col-md-6'>
              {verifyableCredential.nbf !== null ? (
                <p>nbf: {verifyableCredential.nbf}</p>
              ) : null}
            </div>
          </div>
          {verifyableCredential.vc !== null
            ? Object.keys(verifyableCredential.vc.credentialSubject).map(
                (key) => (
                  <div className='row'>
                    <div className='col-md-12'>
                      <p key={key}>
                        {key} : {verifyableCredential.vc.credentialSubject[key]}
                      </p>
                    </div>
                  </div>
                )
              )
            : null}
        </div>
      </div>
      {/* {verifyableCredential.iss === null &&
      (verifyableCredential.nbf === null) &
        (verifyableCredential.vc === null) ? (
        <h1>No file imported yet...</h1>
      ) : null}
      {verifyableCredential.iss !== null ? (
        <h1>iss: {verifyableCredential.iss}</h1>
      ) : null}
      {verifyableCredential.nbf !== null ? (
        <h1>nbf: {verifyableCredential.nbf}</h1>
      ) : null}
      {verifyableCredential.vc !== null
        ? Object.keys(verifyableCredential.vc.credentialSubject).map((key) => (
            <p key={key}>
              {key} : {verifyableCredential.vc.credentialSubject[key]}
            </p>
          ))
        : null} */}
    </div>
  )
}

export default ShowVc
