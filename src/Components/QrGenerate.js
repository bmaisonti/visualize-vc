import { QRCode } from "react-qr-svg"

const QrGenerate = (props) => {
  // const data = props.data
  // const stringdata = JSON.stringify(data)
  return (
    <div className='container-fluid' style={{ padding: "10% 0" }}>
      <div className='row'>
        <div className='col-md-12' style={{ textAlign: "center" }}>
          <QRCode
            level='Q'
            style={{ width: 256 }}
            value={"Wish you happy rich!"}
          />
        </div>
      </div>
    </div>
  )
}

export default QrGenerate
