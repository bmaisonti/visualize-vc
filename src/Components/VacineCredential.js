import { useFormContext } from "react-hook-form"

const VacineCredentialForm = () => {
  const { register } = useFormContext()

  return (
    <div className='container-fluid' style={{ padding: "5% 0" }}>
      <div className='card'>
        <div className='card-body'>
          <h1 style={{ textAlign: "center" }}>Vacine Credential</h1>
          <div className='row'>
            {/* 1st column */}
            <div className='col-md-6'>
              <div className='form-group'>
                <label htmlFor='vaccineEvent'>Vaccine Event</label>
                <input
                  type='text'
                  className='form-control'
                  name='vaccineEvent'
                  defaultValue='1'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='linkedVaccineEvent'>Linked VaccineEvent</label>
                <input
                  type='text'
                  className='form-control'
                  name='linkedVaccineEvent'
                  defaultValue='4d76deb2-8f1c-4a5f-ba9f-a9fbe50d340c'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='recipient'>Recipient</label>
                <input
                  type='text'
                  className='form-control'
                  name='recipient'
                  defaultValue='1234567890123'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='dateOfVaccination'>Date Of Vaccination</label>
                <input
                  className='form-control'
                  type='date'
                  defaultValue='2021-03-09'
                  name='dateOfVaccination'
                  ref={register({ required: true })}
                />
              </div>

              <div className='form-group'>
                <label htmlFor='cvxCode'>Cvx Code</label>
                <input
                  type='text'
                  className='form-control'
                  name='cvxCode'
                  defaultValue='208'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='medicinalProductName'>
                  Medicinal ProductName
                </label>
                <input
                  type='text'
                  className='form-control'
                  name='medicinalProductName'
                  defaultValue='80777-273-10'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='marketingAuthorizationHolder'>
                  Marketing AuthorizationHolder
                </label>
                <input
                  type='text'
                  className='form-control'
                  name='marketingAuthorizationHolder'
                  defaultValue='80777-273-10'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='cycleNumber'>Cycle Number</label>
                <input
                  type='text'
                  className='form-control'
                  name='cycleNumber'
                  defaultValue='1/2'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='administeringCentre'>
                  Administering Centre
                </label>
                <input
                  type='text'
                  className='form-control'
                  name='administeringCentre'
                  defaultValue='สถาบันบำราศนราดูร'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='healthProfessional'>Health Professional</label>
                <input
                  type='text'
                  className='form-control'
                  name='healthProfessional'
                  defaultValue='นางซีโน เวค'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='disease'>Disease</label>
                <input
                  type='text'
                  className='form-control'
                  name='disease'
                  defaultValue='ICD-10'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='certificateIssuer'>Certificate Issuer</label>
                <input
                  type='text'
                  className='form-control'
                  name='certificateIssuer'
                  defaultValue='กระทรวงสาธารณสุข'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='certificateValidFrom'>
                  Certificate Valid From
                </label>
                <input
                  className='form-control'
                  type='date'
                  defaultValue='2021-02-14'
                  name='certificateValidFrom'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='formVersion'>Form Version</label>
                <input
                  type='text'
                  className='form-control'
                  name='formVersion'
                  defaultValue='1.0.mock'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>
            </div>
            {/* 2nd column */}
            <div className='col-md-6'>
              <div className='form-group'>
                <label htmlFor='givenName'>Given Name</label>
                <input
                  type='text'
                  className='form-control'
                  name='givenName'
                  defaultValue='นาย เซอร์ติ'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='middleName'>Middle Name</label>
                <input
                  type='text'
                  className='form-control'
                  name='middleName'
                  defaultValue=''
                  ref={register}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='familyName'>Family Name</label>
                <input
                  type='text'
                  className='form-control'
                  name='familyName'
                  defaultValue='วัคซี'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='birthDate'>Birth Date</label>
                <input
                  className='form-control'
                  type='date'
                  defaultValue='2001-01-01'
                  name='birthDate'
                  ref={register({ required: true })}
                />
              </div>

              <div className='form-group'>
                <label htmlFor='gender'>Gender</label>
                <input
                  type='text'
                  className='form-control'
                  name='gender'
                  defaultValue='1'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='batchNumber'>Batch Number</label>
                <input
                  type='text'
                  className='form-control'
                  name='batchNumber'
                  defaultValue='A2021010041'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='nextVaccinationDate'>
                  Next VaccinationDate
                </label>
                <input
                  className='form-control'
                  type='date'
                  defaultValue='2001-01-01'
                  name='nextVaccinationDate'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='stateOfVaccination'>State Of Vaccination</label>
                <input
                  type='text'
                  className='form-control'
                  name='stateOfVaccination'
                  defaultValue=''
                  ref={register}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='cityOfVaccination'>City Of Vaccination</label>
                <input
                  type='text'
                  className='form-control'
                  name='cityOfVaccination'
                  defaultValue='นนทบุรี'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='countryOfVaccination'>
                  Country Of Vaccination
                </label>
                <input
                  type='text'
                  className='form-control'
                  name='countryOfVaccination'
                  defaultValue='TH'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='vaccineDescription'>Vaccine Description</label>
                <input
                  type='text'
                  className='form-control'
                  name='vaccineDescription'
                  defaultValue='1119305005|1119349007|1142178009'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='atcCode'>Atc Code</label>
                <input
                  type='text'
                  className='form-control'
                  name='atcCode'
                  defaultValue='81901420017654742548'
                  ref={register({ required: true })}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='certificateNumber'>Certificate Number</label>
                <input
                  type='text'
                  className='form-control'
                  name='certificateNumber'
                  defaultValue=''
                  ref={register}
                />
                <small className='form-text text-muted'>Some help text</small>
              </div>

              <div className='form-group'>
                <label htmlFor='certificateValidTo'>Certificate Valid To</label>
                <input
                  className='form-control'
                  type='date'
                  defaultValue='2022-02-14'
                  name='certificateValidTo'
                  ref={register({ required: true })}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default VacineCredentialForm
