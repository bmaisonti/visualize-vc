import { Link } from "react-router-dom"
// import { Navbar, Nav, Icon, Dropdown } from "rsuite"

import Navbar from "react-bootstrap/Navbar"
import Logo1 from "../Public/image/finema.svg"

const NavBar = () => {
  // const test = () => {
  //   console.log(window.location.origin)
  //   // console.log(LOGO1)
  // }
  return (
    <Navbar
      expand='md'
      style={{ backgroundColor: "#01578C", color: "#ECF2F5" }}
    >
      <div className='container'>
        <Navbar.Brand>
          <img
            src={Logo1}
            alt='Test'
            style={{ width: "20%", verticalAlign: "bottom" }}
          />
          <Link to='/' className='navBrandSize'>
            Verification Portal
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls='basic-navbar-nav'
          style={{ backgroundColor: "white" }}
        />
        <Navbar.Collapse id='basic-navbar-nav' className='justify-content-end'>
          <div className='w-75'>
            <div
              className='d-flex nav-sub-text'
              style={{ justifyContent: "space-between", fontWeight: "unset" }}
            >
              <div className='links'>
                <Link to='/'>Home</Link>
              </div>
              <div className='links'>
                <Link to='/showvc'>ShowVC</Link>
              </div>
              <div className='links'>
                <Link to='/vacine-cred'>VacineCredential</Link>
              </div>
            </div>
          </div>
        </Navbar.Collapse>
      </div>
    </Navbar>
  )
}

export default NavBar
