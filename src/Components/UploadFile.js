import React, { useCallback } from "react"
import { useDropzone } from "react-dropzone"
import jwt_decode from "jwt-decode"
import { useDispatch } from "react-redux"
import { setVc } from "../Redux/Actions/Action"
import { useHistory } from "react-router-dom"
import { library } from "@fortawesome/fontawesome-svg-core"
import { fas } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { MOCK_DATA } from "../Redux/Types/Type"
import CerImg from "../Public/image/Certification.svg"
import Dropfile from "../Public/image/Dropfile.svg"
import Logo1 from "../Public/image/finema.svg"

function UploadFile() {
  library.add(fas)

  const dispatch = useDispatch()
  const history = useHistory()

  const onDrop = useCallback((acceptedFiles) => {
    acceptedFiles.forEach((file) => {
      showFile(file)
    })
    // eslint-disable-next-line
  }, [])

  const { getRootProps, getInputProps } = useDropzone({ onDrop })

  const showFile = async (e) => {
    // e.preventDefault()
    const file = e
    if (file) {
      const data = await new Response(file).text()
      var decoded = jwt_decode(data)
      addToReducer(decoded)
    }
  }

  const addToReducer = (data) => {
    const newobj = {
      iss: data.iss,
      nbf: data.nbf,
      vc: {
        credentialSubject: data.vc.credentialSubject,
      },
    }
    dispatch(setVc(newobj))
    history.push("/showvc")
  }

  const dragDiv = (ev) => {
    console.log(ev)
  }

  const allowDropDiv = (ev) => {
    ev.preventDefault()
  }

  const dropDiv = (ev) => {
    console.log("on drop file", ev.dataTransfer.files[0])
    if (ev.dataTransfer.files.length === 0) {
      addToReducer(MOCK_DATA)
    } else {
      showFile(ev.dataTransfer.files[0])
    }
    ev.preventDefault()
  }

  return (
    <div style={{ color: "#ECF2F5" }}>
      <div className='container ' style={{ padding: "3% 0" }}>
        <div className='row'>
          <div className='col-md-5'>
            <h1 style={{ fontWeight: "bolder" }}>
              An easy way to check and verify your certificates
            </h1>
            <p>
              Whether you're a student or an employer, Verification Portal lets
              you verify the certificates you have of anyone from any
              institution. all in one place.
            </p>
            <div className='row'>
              <div
                className='col-md-6'
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <div
                  className='rounded-circle'
                  draggable='true'
                  onDragStart={dragDiv}
                >
                  <img
                    src={CerImg}
                    alt=''
                    className='center-doc-img'
                    style={{ width: "50%", height: "50%" }}
                  />
                </div>
              </div>
              <div className='col-md-6'>
                <div className='upload-arrow-container'>
                  <FontAwesomeIcon
                    style={{ width: "40%", height: "40%" }}
                    icon={["fas", "share"]}
                  />
                  <p>
                    Drag me over there to see a demo certificate and other
                    features
                  </p>
                  <div>
                    <img
                      src={Logo1}
                      alt='Test'
                      style={{ width: "20%", verticalAlign: "bottom" }}
                    />
                    <a href='/' style={{ fontWeight: "bolder" }}>
                      Verification Portal
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-md-7'>
            <div className='outline-drop'>
              <div
                className='drop-container'
                onDrop={dropDiv}
                onDragOver={allowDropDiv}
              >
                <div style={{ width: "75%" }}>
                  <input
                    type='text'
                    className='form-control adj-input-url'
                    name='stateOfVaccination'
                    placeholder='Paste your document address here'
                  />
                </div>
                <p className='between-text'>
                  <span className='between-text2'>or</span>
                </p>
                <div
                  {...getRootProps()}
                  style={{
                    width: "100%",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <input {...getInputProps()} />
                  <img src={Dropfile} alt='Dropfile' style={{ width: "20%" }} />
                  <h3 style={{ color: "black" }}>Drop your file</h3>
                  <p style={{ color: "darkgray" }}>to views it contents</p>
                  <p className='between-text'>
                    <span className='between-text2'>or</span>
                  </p>
                  <button type='button' class='btn btn-outline-info'>
                    Select File
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
export default UploadFile
