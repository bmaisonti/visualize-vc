import { useState, useEffect } from "react"
import QrGenerate from "../Components/QrGenerate"
import VacineCredentialForm from "../Components/VacineCredential"
import { useForm, FormProvider, useFormContext } from "react-hook-form"

const FormAndQr = () => {
  const [data, setData] = useState("")
  const methods = useForm()

  const onSubmit = (data) => {
    setVacineData(data)
  }

  const setVacineData = (vacData) => {
    setData(vacData)
  }

  useEffect(() => {}, [data])
  return (
    <FormProvider className='container' {...methods}>
      <form className='row' onSubmit={methods.handleSubmit(onSubmit)}>
        <div className='col-md-8'>
          <VacineCredentialForm />
        </div>
        <div className='col-md-4'>
          <div style={{ padding: "3.2% 0", position: "fixed", width: "30%" }}>
            <div className='card'>
              <div className='card-body'>
                <div className='row'>
                  <div className='col-md-12' style={{ textAlign: "center" }}>
                    <button className='btn btn-success' type='submit'>
                      Generate QR
                    </button>
                  </div>
                </div>
                {data === "" ? null : <QrGenerate data={data} />}
              </div>
            </div>
          </div>
        </div>
      </form>
    </FormProvider>
  )
}

export default FormAndQr
