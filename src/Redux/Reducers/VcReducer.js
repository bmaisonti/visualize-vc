import { SET_VC } from "../Types/Type"

const initialState = {
  iss: null,
  nbf: null,
  vc: null,
}

export const vcReducers = (state = initialState, action) => {
  switch (action.type) {
    case SET_VC:
      console.log(action)
      return {
        ...state,
        iss: action.payload.iss,
        nbf: action.payload.nbf,
        vc: action.payload.vc,
      }
    default:
      return state
  }
}
