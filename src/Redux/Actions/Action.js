import { SET_VC } from "../Types/Type"

export const setVc = (data) => ({
  type: SET_VC,
  payload: data,
})
